(function ($) {
	$(document).ready(function() {

		$('#products').on('click', function(event) {
			$('.menu ul').toggleClass('open');
		});


		$('.slider').owlCarousel({
            items: 1,
            mouseDrag:false,
            loop:true,
            autoplay: true,
            autoplayTimeout:7500,
            responsive : {
    			0 : {
    			    items:1
    			}
			}
        });

        $('.headerHam').on('click', function(event) {
			if ($('.navigationBack').css('display')=='block') {
				$('.navigationBack').css({
					display: 'none'
				});
			} else {
				$('.navigationBack').css({
					display : 'inline-block'
				});
			}
		});

		$('.navigationBack li a').on('click', function() {
			$('.navigationBack').css({
				display: 'none'
			});
		});

		$('div.products').click(function() {
			if ($('.drop').css('display')=='block') {
				$('.drop').css({
					display: 'none'
				});
			}else {
				$('.drop').css({
					display: 'inline-block'
				});
			}
		});




	});
})(jQuery);